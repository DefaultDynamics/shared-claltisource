﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Data;
using System.Linq;
using System.Net;
using System.ServiceModel.Dispatcher;
using System.ServiceModel.Description;
using System.Text;
using iTextSharp.text;
using iTextSharp.text.html.simpleparser;
using iTextSharp.text.pdf;

namespace CLAltiSource
{
   public class AltiSource : CLExternalNotifications.INotification, CLExternalNotifications.IInvoice
   {
      /*
      public class MyMessageInspector : IClientMessageInspector
      { 
      }

      public class InspectorBehavior : IEndpointBehavior
      {
         public void ApplyClientBehavior(ServiceEndpoint endpoint, ClientRuntime clientRuntime)
         {
            clientRuntime.MessageInspectors.Add(new MyMessageInspector());
         }
      }
      */

      private class MyMemoryStream : MemoryStream
      {
         protected override void Dispose(bool disposing)
         {
            disposing = disposing && true;
         }

         public void MyDispose(bool disposing)
         {
            base.Dispose(disposing);
         }
      }

      public static Stream Create(string HTMLCode)
      {
         MyMemoryStream tmpFile = new MyMemoryStream();
         Document Document;
         StringReader str;
         HTMLWorker htmlworker;
         string ordHtml = "";
         ordHtml = HTMLCode;
         Document = new Document();
         //PdfWriter.GetInstance(Document, new FileStream(pdfFileName, FileMode.Create));
         PdfWriter.GetInstance(Document, tmpFile);
         Document.Open();
         str = new StringReader(ordHtml);
         htmlworker = new HTMLWorker(Document, "");
         htmlworker.Parse(str);
         Document.Close();
         htmlworker.Close();
         MemoryStream pdfFile = new MemoryStream();
         tmpFile.Position = 0;
         tmpFile.CopyTo(pdfFile);
         tmpFile.MyDispose(true);
         pdfFile.Flush();
         pdfFile.Position = 0;
         return pdfFile;
      }

      private AltiSourceWS.updateAspsAuctionForeClosureOrder CreateMessage(string OrderId)
      {
         AltiSourceWS.updateAspsAuctionForeClosureOrder AsMessage = null;
         AsMessage = new AltiSourceWS.updateAspsAuctionForeClosureOrder
         {
            Message = new AltiSourceWS.Message
            {
               Header = new AltiSourceWS.Header
               {
                  LoginAccountIdentifier =  Properties.Settings.Default.AltiSourceUserId, //System.Web.Configuration.WebConfigurationManager.AppSettings["AltiSourceUserId"],
                  LoginAccountPassword = Properties.Settings.Default.AltiSourcePassword // System.Web.Configuration.WebConfigurationManager.AppSettings["AltiSourcePassword"],
               },
               Body = new AltiSourceWS.Body
               {
                  Order = new AltiSourceWS.Order
                  {
                     action = AltiSourceWS.OrderAction.UPDATE,
                     transactionID = OrderId
                  }
               }
            }
         };
         return AsMessage;
      }

      private decimal fixDecimal(decimal? value)
      {
         return fixDecimal(value, 0);
      }

      private decimal fixDecimal(decimal? value, decimal defaultValue)
      {
         return (value.HasValue ? value.Value : defaultValue);
      }

      private DateTime fixDateTime(DateTime? value)
      {
         return fixDateTime(value, DateTime.Today);
      }

      private DateTime fixDateTime(DateTime? value, DateTime defaultValue)
      {
         return (value.HasValue ? value.Value : defaultValue);
      }

      public string sentInvoiceNotification(string docBasePath, int orderId)
      {
         CLBusiness.Data.CLDataDataContext data = new CLBusiness.Data.CLDataDataContext();
         CLBusiness.Data.tblOrder ord = data.tblOrders.Where(o => o.ordNumber == orderId).FirstOrDefault();
         CLBusiness.Data.tblOrder_Foreclosure ford = data.tblOrder_Foreclosures.Where(fo => fo.ordNumber == orderId).FirstOrDefault();
         IQueryable<CLBusiness.Data.tblOrder_Document> oDocs = data.tblOrder_Documents.Where(od => od.ordNumber == orderId); //filter

         AltiSourceWS.updateAspsAuctionForeClosureOrder AsMessage = CreateMessage(ord.ordFileNumber);
         AltiSourceWS.InvoiceAndCompleteFCAuctionType IAC = new AltiSourceWS.InvoiceAndCompleteFCAuctionType
         {
            InvoiceAmount = 25,//ford.ordForeclosureSalePrice.Value,
            Attachment = new AltiSourceWS.Attachment[1],
            Comments = ""
         };

         string strBody = 
            "<html> "+
            "  <head></head> " + 
            "  <body> " + 
            "   This is a test invoice for $25 for the following property " + ord.ordAddress +
            "  </body> " + 
            "</html>";
         Stream s = Create(strBody);

         int iDoc = 0;

         IAC.Attachment[0] = new AltiSourceWS.Attachment
         {
            type = "pdf",
            name = "Invoice",
            Checksum = "",
            Payload = new byte[s.Length]
         };

         s.Read(IAC.Attachment[iDoc].Payload, 0, (int)s.Length);

         AsMessage.Message.Body.Order.InvoiceAndCompleteFCAuction = IAC;

         AltiSourceWS.AspsAuctionForeclosureVendorOrderWSService AsWs = new AltiSourceWS.AspsAuctionForeclosureVendorOrderWSService();
         AltiSourceWS.updateAspsAuctionForeClosureOrderResponse r = AsWs.updateAspsAuctionForeClosureOrder(AsMessage);
         if (r.Message.Body == null)
         {
            CLBusiness.CLCommon.EventLogWrite("Error when InvoiceAndCompleteFCAuction to Altisource: " + r.Message.Header.Message, System.Diagnostics.EventLogEntryType.Error);
            return r.Message.Header.Message;
         }
         else
            return null;

      }

      public string sentSaleNotifation(string docBasePath, int orderId, string[] docList)
      {
         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 1 " + docBasePath, System.Diagnostics.EventLogEntryType.Information);
         CLBusiness.Data.CLDataDataContext data = new CLBusiness.Data.CLDataDataContext();
         CLBusiness.Data.tblOrder ord = data.tblOrders.Where(o => o.ordNumber == orderId).FirstOrDefault();
         IQueryable<CLBusiness.Data.tblOrder_Document> oDocs = data.tblOrder_Documents.Where(od => od.ordNumber == orderId); //filter
         CLBusiness.Data.tblOrder_Foreclosure ford = data.tblOrder_Foreclosures.Where(fo => fo.ordNumber == orderId).FirstOrDefault();
         IQueryable<CLBusiness.Data.tblOrder_ForeclosurePayment> fordps = data.tblOrder_ForeclosurePayments.Where(fop => fop.ordNumber == orderId);

         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 2 " + ord.ordFileNumber, System.Diagnostics.EventLogEntryType.Information);
         AltiSourceWS.updateAspsAuctionForeClosureOrder AsMessage = CreateMessage(ord.ordFileNumber);
         AltiSourceWS.ForeclosureAuctionResultsType far = new AltiSourceWS.ForeclosureAuctionResultsType
         {
            AuctionResults = 
               ford.ordForeclosureStatus=="BUYER" ?  AltiSourceWS.AuctionResultsValues.THIRDPARTYSALE : AltiSourceWS.AuctionResultsValues.BACKTOTHEBANK,
            Offer = new AltiSourceWS.OfferType
            {
               HowManyBids = 0,
               WinningBid = fixDecimal(ford.ordForeclosureSalePrice, 0),
               ClosingDate = ord.ordClosingDate.Value,
               BidOpenDate = fixDateTime(ford.ordForeclosureBidOpenTime, DateTime.MinValue),
               BidOpenDateSpecified = true,
               BidOpenCoordinates = ford.ordForeclosureBidOpenLocation ?? "",
               BidCloseCoordinates = ford.ordForeclosureBidCloseLocation ?? ""
            }
         };

         if (ford.ordForeclosureStatus == "BUYER")
            far.Offer.BuyerInfo = new AltiSourceWS.BuyerInfoType
            {
               BuyerType = AltiSourceWS.BuyerTypeValues.OTHER,
               Purchaser = ford.ordForeclosurePurchaser,
               BuyerPhone = ford.ordForeclosurePurchaser_Telephone,
               BuyerAddress = ford.ordForeclosurePurchaser_Address,
               BuyerCity = ford.ordForeclosurePurchaser_City,
               BuyerState = (AltiSourceWS.StateValues)Enum.Parse(typeof(AltiSourceWS.StateValues), ford.ordForeclosurePurchaser_State),
               BuyerStateSpecified = true,
               BuyerPostal = ford.ordForeclosurePurchaser_Zip,
               BuyerEmail = ford.ordForeclosurePurchaser_Email,
               TitleAsShown = ford.ordForeclosureTitleAsShown,
               DeedMailTo = ford.ordForeclosureDeedMailTo,
               AuctionVerificationAttachments = new AltiSourceWS.Attachment[oDocs.Count() + (docList == null ? 0 : docList.Count())],
               PurchaserRepresentative = new AltiSourceWS.Person
               {
                  LastName = ford.ordForeclosureRepresentative ?? "",
                  Email = ford.ordForeclosureRepresentative_Email ?? "",
                  PrimaryPhone = ford.ordForeclosureRepresentative_Telephone ?? "",
                  Fax = "N/A",
                  Address =
                     (ford.ordForeclosureRepresentative_City == null) ?
                        new AltiSourceWS.Address[] { } :
                        new AltiSourceWS.Address[] { 
						         new AltiSourceWS.Address {
							         City = ford.ordForeclosureRepresentative_City??"",
							         PostalCode = ford.ordForeclosureRepresentative_Zip??"",
							         State = ford.ordForeclosureRepresentative_State??"",
							         Street1 = ford.ordForeclosureRepresentative_Address??"",
							         Street2 = null } }
               },
               RepresentativeRelationshipToPurchaser = ford.ordForeclosureRepresentative_Relationship
            };
         else
            far.Offer.BuyerInfo = new AltiSourceWS.BuyerInfoType
            {
               BuyerType = AltiSourceWS.BuyerTypeValues.OTHER,
               Purchaser = "",
               BuyerPhone = "",
               BuyerAddress = "",
               BuyerCity = "",
               BuyerState = AltiSourceWS.StateValues.GA,
               BuyerPostal = "",
               BuyerEmail = "",
               TitleAsShown = "",
               DeedMailTo = "",
               AuctionVerificationAttachments = new AltiSourceWS.Attachment[oDocs.Count() + (docList == null ? 0 : docList.Count())],
               PurchaserRepresentative = new AltiSourceWS.Person
               {
                  LastName = "",
                  Email = "",
                  PrimaryPhone = "",
                  Fax = "",
                  Address = new AltiSourceWS.Address[] { }
               },
               RepresentativeRelationshipToPurchaser = ""
            };
         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 3", System.Diagnostics.EventLogEntryType.Information);

         int iDoc = 0;
         foreach (CLBusiness.Data.tblOrder_Document oDoc in oDocs)
         {
            string docPath = docBasePath + oDoc.docLocation.Replace('/', '\\') + "\\" + oDoc.docName;
            System.IO.Stream fDoc = null;
            long docLength = 0;
            if (oDoc.docExternalLocation != null && oDoc.docExternalLocation.Length > 0)
            {
               WebRequest request = WebRequest.Create(oDoc.docExternalLocation + oDoc.docName);
               request.Timeout = 30 * 60 * 1000;
               request.UseDefaultCredentials = true;
               request.Proxy.Credentials = request.Credentials;
               WebResponse response = (WebResponse)request.GetResponse();
               fDoc = response.GetResponseStream();
               docLength = response.ContentLength;
               //using (Stream s = response.GetResponseStream())
               //{
               //   s.CopyTo(Response.OutputStream);
               //}
            }
            else
            {
               CLBusiness.CLCommon.EventLogWrite(docPath, System.Diagnostics.EventLogEntryType.Information);
               System.IO.FileStream tmpDoc = new System.IO.FileStream(docPath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
               docLength = tmpDoc.Length;
               fDoc = tmpDoc;
            }
            far.Offer.BuyerInfo.AuctionVerificationAttachments[iDoc] = new AltiSourceWS.Attachment
            {
               type = System.IO.Path.GetExtension(oDoc.docName),
               name = System.IO.Path.GetFileName(oDoc.docName),
               Checksum = "",
               Payload = new byte[docLength]
            };
            fDoc.Read(far.Offer.BuyerInfo.AuctionVerificationAttachments[iDoc].Payload, 0, (int)docLength);
            iDoc += 1;
         }
         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 4", System.Diagnostics.EventLogEntryType.Information);

         if (docList != null)
            foreach (string docPath in docList)
            {
               System.IO.FileStream fDoc = new System.IO.FileStream(docPath, System.IO.FileMode.Open, System.IO.FileAccess.Read);
               far.Offer.BuyerInfo.AuctionVerificationAttachments[iDoc] = new AltiSourceWS.Attachment
               {
                  type = System.IO.Path.GetExtension(docPath),
                  name = System.IO.Path.GetFileName(docPath),
                  Checksum = "",
                  Payload = new byte[fDoc.Length]
               };
               fDoc.Read(far.Offer.BuyerInfo.AuctionVerificationAttachments[iDoc].Payload, 0, (int)fDoc.Length);
               iDoc += 1;
            }
         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 5", System.Diagnostics.EventLogEntryType.Information);

         if (fordps.Count() > 0)
         {
            decimal total = 0;
            far.Offer.PaymentInfo = new AltiSourceWS.PaymentInfoType();
            far.Offer.PaymentInfo.ConductedBy = "My Self";
            far.Offer.PaymentInfo.Payment = new AltiSourceWS.PaymentType[fordps.Count()];
            int iPay = 0;
            foreach (CLBusiness.Data.tblOrder_ForeclosurePayment fordp in fordps)
            {
               AltiSourceWS.PaymentType asPayment = new AltiSourceWS.PaymentType();
               asPayment.PaymentID = fordp.ordForeclosureChkId.ToString();
               switch (fordp.ordForeclosureChkNumber)
               {
                  case "CASH":
                     asPayment.PaymentType1 = AltiSourceWS.PaymentTypeValues.CASH;
                     asPayment.CheckNumber = "";
                     break;
                  case "CREDIT":
                     asPayment.PaymentType1 = AltiSourceWS.PaymentTypeValues.CREDIT;
                     asPayment.CheckNumber = "";
                     break;
                  default:
                     asPayment.PaymentType1 = AltiSourceWS.PaymentTypeValues.CHECK;
                     asPayment.CheckNumber = fordp.ordForeclosureChkNumber;
                     break;
               }
               total += fordp.ordForeclosureChkAmount;
               asPayment.PaymentAmount = fordp.ordForeclosureChkAmount;
               asPayment.Institution = fordp.ordForeclosureInstitution;
               far.Offer.PaymentInfo.Payment[iPay] = asPayment;
               iPay += 1;
            }
            far.Offer.PaymentInfo.TotalPaymentsAmount = total;
            far.Offer.PaymentInfo.RefundDue = total-(ford.ordForeclosureSalePrice??0);
            far.Offer.PaymentInfo.RefundDueSpecified = true;
            far.Offer.PaymentInfo.RefundPayableTo = ford.ordForeclosureRefundPayableTo;
            far.Offer.PaymentInfo.Form8300 = "N/A";
            far.Offer.PaymentInfo.Comments = "N/A";
         }
         else
         {
            far.Offer.PaymentInfo = new AltiSourceWS.PaymentInfoType
            {
               ConductedBy = "",
               Payment = new AltiSourceWS.PaymentType[] {
                  new AltiSourceWS.PaymentType {
                     PaymentType1 = AltiSourceWS.PaymentTypeValues.CASH,
                     CheckNumber = "",
                     Institution = "",
                     PaymentAmount = 0,
                     PaymentID = "0"
                  }
               },
               RefundDue = 0,
               RefundPayableTo = "N/A",
               Form8300 = "N/A",
               Comments = "N/A"
            };
         }
         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 6", System.Diagnostics.EventLogEntryType.Information);

         AsMessage.Message.Body.Order.ForeclosureAuctionResults = far;

         AltiSourceWS.AspsAuctionForeclosureVendorOrderWSService AsWs = new AltiSourceWS.AspsAuctionForeclosureVendorOrderWSService();
         AltiSourceWS.updateAspsAuctionForeClosureOrderResponse r = AsWs.updateAspsAuctionForeClosureOrder(AsMessage);
         CLBusiness.CLCommon.EventLogWrite("sentSaleNotifation 6", System.Diagnostics.EventLogEntryType.Information);
         if (r.Message.Body == null)
         {
            CLBusiness.CLCommon.EventLogWrite("Error when sentSaleNotifation to Altisource: " + r.Message.Header.Message, System.Diagnostics.EventLogEntryType.Error);
            return r.Message.Header.Message;
         }
         else
            return null;
      }
   }
}
